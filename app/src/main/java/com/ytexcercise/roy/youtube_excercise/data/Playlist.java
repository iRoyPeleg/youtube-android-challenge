package com.ytexcercise.roy.youtube_excercise.data;

/**
 * Created by Roy on 6/16/2016.
 */
public class Playlist {

    private String title;
    private YTVideo[] ytVideos;

    public Playlist() {
    }

    public Playlist(String title, YTVideo[] ytVideos) {
        this.title = title;
        this.ytVideos = ytVideos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public YTVideo[] getYtVideos() {
        return ytVideos;
    }

    public void setYtVideos(YTVideo[] ytVideos) {
        this.ytVideos = ytVideos;
    }
}
