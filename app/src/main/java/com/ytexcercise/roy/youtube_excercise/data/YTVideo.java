package com.ytexcercise.roy.youtube_excercise.data;

/**
 * Created by Roy on 6/16/2016.
 */
public class YTVideo {

    private String name;
    private String link;
    private String thumb;

    public YTVideo(String name, String link, String thumb) {
        this.name = name;
        this.link = link;
        this.thumb = thumb;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
