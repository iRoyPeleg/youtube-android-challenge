package com.ytexcercise.roy.youtube_excercise.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ytexcercise.roy.youtube_excercise.R;
import com.ytexcercise.roy.youtube_excercise.adapter.PlaylistAdapter;
import com.ytexcercise.roy.youtube_excercise.data.Playlist;
import com.ytexcercise.roy.youtube_excercise.data.jsonParse;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private Playlist[] mPlaylists;
    public jsonParse mJSONParser = new jsonParse("http://www.razor-tech.co.il/hiring/youtube-api.json");
    private JSONObject json;
    //public TheTask task= new TheTask();

    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        // Getting JSON Object
        mJSONParser.execute();
        json = mJSONParser.getJsonObject();


        PlaylistAdapter adapter = new PlaylistAdapter(this, mPlaylists);
        mRecyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
    }


}
