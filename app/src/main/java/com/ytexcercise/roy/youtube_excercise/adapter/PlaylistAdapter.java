package com.ytexcercise.roy.youtube_excercise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ytexcercise.roy.youtube_excercise.R;
import com.ytexcercise.roy.youtube_excercise.data.Playlist;

import javax.xml.transform.Templates;

/**
 * Created by Roy on 6/16/2016.
 */
/**
 * Created by RoyPeleg on 07/10/15.
 */
public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder> {

    private Playlist[] mPlaylists;
    private Context mContext;
    public PlaylistAdapter(Context context, Playlist[] playlists){
        mContext=context;
        mPlaylists = playlists;
    }
    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.playlist_list_item,parent,false);
        PlaylistViewHolder viewHolder = new PlaylistViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlaylistViewHolder holder, int position) {
        holder.bindTitle(mPlaylists[position]);
    }

    @Override
    public int getItemCount() {
        return mPlaylists.length;
    }

    public class PlaylistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView mPlaylistTitle;

        public PlaylistViewHolder(View itemView){
            super(itemView);

            mPlaylistTitle = (TextView) itemView.findViewById(R.id.playlistName);

            itemView.setOnClickListener(this);
        }


        public void bindTitle(Playlist pl){
            mPlaylistTitle.setText(pl.getTitle());
        }

        @Override
        public void onClick(View v) {
            //String time = mTimeLabel.getText().toString();
            //String temperature = mTemperatureLabel.getText().toString();
            //String summary = mSummaryLabel.getText().toString();
            //String message = String.format("On %s the high will be %s and it will be %s",time,temperature, summary);
            //Toast.makeText(mContext,message,Toast.LENGTH_LONG).show();
        }
    }
}
