package com.ytexcercise.roy.youtube_excercise.data;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Roy on 6/16/2016.
 */
public class jsonParse extends AsyncTask<String, String, JSONObject> {

    private JSONObject reader;
    public String mURL = null;
    static InputStream is = null;
    public static JSONObject jObj = null;
    private JSONObject jsonObject = null;
    static String json = "";

    // constructor
    public jsonParse(String url) {
        mURL = url;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        return getJSONFromUrl(mURL);
    }

    // @Override
    protected void onPostExecute(JSONObject result) {

        jsonObject = result;

    }

    // function get json from url
// by making HTTP POST or GET method
    protected JSONObject getJSONFromUrl(String url) {

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            } catch (ClientProtocolException e) {
            e.printStackTrace();
            } catch (IOException e) {
            e.printStackTrace();
            }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
                }
            is.close();
            json = sb.toString();
            } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
            } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

        // return JSON String
        return jObj;


    }


    public Playlist[] jsonParsePlaylists(String strJson) {

            try {
                JSONObject jsonRootObject = new JSONObject(strJson);

                //Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = jsonRootObject.optJSONArray("Playlists");

                int arrLength = jsonArray.length();
                Playlist mPlaylists[] = new Playlist[arrLength];

                //Iterate the jsonArray and print the info of JSONObjects
                for (int i = 0; i < arrLength; i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    mPlaylists[i].setTitle(jsonObject.optString("ListTitle").toString());


                    // Insert call to method parseSongs
                    //mPlaylists[i].setYtVideos(parseSongs());
                }

                return (mPlaylists);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        return null;
    }

    public YTVideo[] parseSongs (String strJson){


        try {
            JSONObject  jsonRootObject = new JSONObject(strJson);

            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonArray = jsonRootObject.optJSONArray("ListItems");

            int arrLength = jsonArray.length();
            String playlistNames[] = new String[arrLength];
            YTVideo[] ytVideos = new YTVideo[arrLength];

            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < arrLength; i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                ytVideos[i].setName(jsonObject.optString("Title").toString());
                ytVideos[i].setLink(jsonObject.optString("link").toString());
                ytVideos[i].setThumb(jsonObject.optString("thumb").toString());
            }

            return (ytVideos);
        } catch (JSONException e) {e.printStackTrace();}



        return null;

    }






}
