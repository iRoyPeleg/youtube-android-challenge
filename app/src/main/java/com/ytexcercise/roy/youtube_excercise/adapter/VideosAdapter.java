package com.ytexcercise.roy.youtube_excercise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ytexcercise.roy.youtube_excercise.R;
import com.ytexcercise.roy.youtube_excercise.data.Playlist;
import com.ytexcercise.roy.youtube_excercise.data.YTVideo;

/**
 * Created by Roy on 6/16/2016.
 */

/**
 * Created by RoyPeleg on 07/10/15.
 */
public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.VideoViewHolder> {

    private YTVideo[] mVideos;
    private Context mContext;
    public VideosAdapter(Context context, YTVideo[] videos){
        mContext = context;
        mVideos = videos;
    }
    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_item,parent,false);
        VideoViewHolder viewHolder = new VideoViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder, int position) {
        holder.bindTitle(mVideos[position]);
    }

    @Override
    public int getItemCount() {
        return mVideos.length;
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView mSongTitle;
        public ImageView mSongThumb;

        public VideoViewHolder(View itemView){
            super(itemView);

            mSongTitle = (TextView) itemView.findViewById(R.id.songName);
            mSongThumb = (ImageView) itemView.findViewById(R.id.videoThumb);

            itemView.setOnClickListener(this);
        }


        public void bindTitle(YTVideo video){
            mSongTitle.setText(video.getName());

            //TODO: Set The Image Download
            //mSongThumb.setText(video.getName());
        }

        @Override
        public void onClick(View v) {

            // TODO: Set new intent with action.

            //String time = mTimeLabel.getText().toString();
            //String temperature = mTemperatureLabel.getText().toString();
            //String summary = mSummaryLabel.getText().toString();
            //String message = String.format("On %s the high will be %s and it will be %s",time,temperature, summary);
            //Toast.makeText(mContext,message,Toast.LENGTH_LONG).show();
        }
    }
}
